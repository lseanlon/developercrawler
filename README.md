
Folder Structure
-----------------------------
crawl   - crawl data related github and slideshare
sql   - contains statement for table structure
shared   - contains shared data folder
application - contains application to list webpage and put into database
Actually runGithub1.sh to runGithub6.sh. is just additional.
Already have in 1 shell.

Running shell programs
-----------------------------
1) One single shell version all [include curl]
runGithub.sh
runSlideShare.sh

2) Separate MultiShell version 
sh runGithub1.sh
sh runGithub2.sh
sh runGithub3.sh
sh runGithub4.sh
sh runGithub5.sh
sh runGithub6.sh
sh runGithub7.sh [curl included]


sh runSlideShare1.sh
sh runSlideShare2.sh
sh runSlideShare3.sh
sh runSlideShare4.sh
sh runSlideShare5.sh [curl included]

GITHUB
-----------------------------
1) Parameter url 
src/configuration/UrlList.txt
a)General Format : full-file-name|url|reference
eg./dist/EntryPage/Github.txt|https://github.com/search?p=100&q=created%3A<[TODAY_DATE]&type=Users&utf8=✓|Github

b)Url Format: 
https://help.github.com/articles/search-syntax/
eg.https://github.com/search?utf8=✓&q=created%3A2017-01-29..2017-02-01++location%3Ajapan&type=Users&ref=searchresults

Or

https://github.com/search?utf8=✓&q=created%3A(ADD,[TODAY_DATE],-3)..[TODAY_DATE]++location%3Ajapan&type=Users&ref=searchresults

c) Today Date format:
[TODAY_DATE]
d) Today date format with ADDITION TO DAYS:
(ADD,[TODAY_DATE],-3)
(ADD,[TODAY_DATE],+120)

2) Parameter Year of Contribution
a)FORMAT: fromDate-toDate
eg.2016-2017
b)Dynamic Token :[THISYEAR]
eg.2016-[THISYEAR]

SLIDESHARE
-----------------------------
1) Parameter url
a)General Format : full-file-name|url|reference
eg./dist/EntryPage/SlideShare.txt|http://www.slideshare.net/featured/category/software|SlideShare
 

2) Parameter Pages
src/configuration/SlideShareMaxPaginationScroll.txt 

specify No of pages to scroll down.