<?php


require 'Slim/Slim.php';
\Slim\Slim::registerAutoloader();
$app = new \Slim\Slim(); 
require_once 'lib/corefunction.php';

$app->get('/', function( ) use ($app)
{ 
 
   	require_once 'lib/mysql.php';
	 

 	$mode = $app->request()->get('mode'); 

 	if(!$mode || isComparedEqual($mode, 'VIEW-ALL')  ){
 			listGithubUsers($app)  ;
 	}

 	else if(  isComparedEqual($mode, 'VIEW-GITHUB')  ){
 			listGithubUsers($app)  ;
 	}
 	else if(  isComparedEqual($mode, 'VIEW-SLIDESHARE')  ){
 			listSlideShareUsers($app)  ;
 	}
 	else if( isComparedEqual($mode, 'UPDATE-SLIDESHARE')  ){
 			updateSlideShareUsers($app) ;
 	}
 	else if( isComparedEqual($mode, 'UPDATE-GITHUB')  ){
 			updateGithubUsers($app) ;
 	}
 	else if( isComparedEqual($mode, 'UPDATE-GITHUB-CONTRIBUTION')  ){
 			updateGithubUsersContributions($app) ;
 	}
 	else {
 			listGithubUsers($app)  ;
 	}


});
 
 

$app->run();

?> 