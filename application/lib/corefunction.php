
<?php
function isComparedEqual($str1, $str2)
{
    return (strcasecmp(($str1), $str2) == 0);
}
function connectDB()
{
    require_once 'lib/mysql.php';
    $db = connect_db();
    return $db;
}
function queryDB($statement)
{
    $db   = connectDB();
    $rs   = $db->query($statement);
    $data = $rs->fetch_all(MYSQLI_ASSOC);
    return $data;
}


function crudDB($statement)
{
    $db = connectDB();
    if ($db->query($statement) === TRUE) {
        return $db;
    } else {
        // echo " Error: " . $sql . "<br>" . $conn->error;
        return false;
    }
    
}

function safeText($rawVal){ 
    if(empty($rawVal)){
        return "";
    }
    $rawVal = str_ireplace("\n", '  ', $rawVal);
    $rawVal = str_ireplace("  ", ' ', $rawVal); 
    return $rawVal ;
}
function updateSlideShareUsers($app)
{
    $db = connect_db();
    
    //read json files
    //loop records
    //read the json file contents
    //$jsondata = file_get_contents('../shared/SlideShareUser-SlideShare.txt');


$myfile = fopen('../shared/SlideShareUser-SlideShare.txt', "r") or die("Unable to open file!");
$jsondata = fread($myfile,filesize('../shared/SlideShareUser-SlideShare.txt'));
fclose($myfile);


    // var_dump($jsondata  ) ;  
    
    //convert json object to php associative array
    $list = json_decode($jsondata, true);
    for ($x = 0; $x <= count($list); $x++) {
        $curRow   = $list[$x];
        $username = $curRow["curSlideAuthor"];
        $username = str_ireplace("http://www.slideshare.net/", "", $username);
        echo "<h3> $x)The name is:   $username  </h3>";
        
        if (empty($username)) {
            return;
        }
        //check duplicate 
        $selectQuery    = 'SELECT id  FROM  ' . $GLOBALS['developertable'];
        $whereStatement = " where 1=1   ";
        
        $ref = $curRow["ref"];
        if (!empty($ref)) {
            $whereStatement = $whereStatement . " and source = '" . $ref . "' ";
        }
        
        if (!empty($username)) {
            $whereStatement = $whereStatement . " and username = '" . $username . "' ";
        }
        echo "<br> $selectQuery . $whereStatement ";
        $result  = $db->query($selectQuery . $whereStatement);
        $curdate = date('Y-m-d H:i:s');
         
        
        
        $ref        = safeText($db->real_escape_string($curRow["ref"]));
        $fullname   = safeText($db->real_escape_string($curRow["curSlideAuthor"]));
        $company    = safeText($db->real_escape_string(''));
        $email      = safeText($db->real_escape_string(''));
        $location   = safeText($db->real_escape_string($curRow["location"]));

        $specialist = safeText($db->real_escape_string($curRow["industry"])); 
        $facebook   = safeText($db->real_escape_string($curRow["facebook"]));
        $linkedin   = safeText($db->real_escape_string($curRow["linkedin"]));
        $twitter    = safeText($db->real_escape_string($curRow["twitter"]));
        
        $totalDocument           = safeText($db->real_escape_string($curRow["totalDocument"]));
        $totalInfographics       = safeText($db->real_escape_string($curRow["totalInfographics"]));
        $totalPresentationSlides = safeText($db->real_escape_string($curRow["totalPresentationSlides"]));
        $totalVideo              = safeText($db->real_escape_string($curRow["totalVideo"]));
        
        $presentationSlidesContent = safeText($db->real_escape_string($curRow["presentationSlidesContent"]));
        $presentationSlidesLink    = safeText($curRow["presentationSlidesLink"]); 
        $presentationSlidesLink    = str_ireplace("'", "\'", $presentationSlidesLink);
        $presentationSlidesContent = $db->real_escape_string(str_ireplace("'", "\'", $presentationSlidesContent));
        
        if ($result->num_rows > 0) {
            
            echo "<h1>Skipped insertion. Duplicate record exists for [ref//username] = " . $ref . "//" . $username . "</h1>";
        } else {
            //insert json into sql  
            $statement    = "INSERT INTO " . $GLOBALS['developertable'] . "(source, fullname, username, company, email, location, specialist, linkedin,facebook,twitter, totalDocument, totalInfographics ,totalVideo, totalPresentationSlides,createdby,createddate)
                VALUES('$ref', '$fullname', '$username', '$company','$email', '$location','$specialist', '$linkedin','$facebook','$twitter','$totalDocument','$totalInfographics','$totalVideo','$totalPresentationSlides','*',  '$curdate')";
            $resultInsert = crudDB($statement);
            $developerid  = !empty($db->insert_id) ? $db->insert_id : $resultInsert->insert_id;
            if (!empty($developerid)) {
                echo "<h3>" . $statement . "</h3>";
                echo " <h1>Performed succcess insertion for [ref//username] = " . $ref . "//" . $username . " >insert_id=" . $developerid . "</h1>";
                
                //insertt into dependent table 
                
                $statement          = "INSERT INTO " . $GLOBALS['developerslidetable'] . "( developerid, slidecontent, slidelinks,createdby,createddate)
                    VALUES( '$developerid', '$presentationSlidesContent', '$presentationSlidesLink', '*',  '$curdate')";
                $resultInsertSlides = crudDB($statement);
                echo " <br/> $statement ";
                
                $resultId = $db->insert_id;
                if (!$resultId) {
                    echo " <h3>Performed succcess insertion slides for [developerid//username] = " . $developerid . "//" . $username . "  " . "</h3>";
                }
                
                
                
                
            }
        }
        
    }
}

function updateGithubUsers($app)
{
    $db = connect_db();
    
    //read json files
    //loop records
    //read the json file contents
    //$jsondata = file_get_contents('../shared/User-Github.txt');


$myfile = fopen('../shared/User-Github.txt', "r") or die("Unable to open file!");
$jsondata = fread($myfile,filesize('../shared/User-Github.txt'));
fclose($myfile);



    // var_dump($jsondata  ) ;  
    
    //convert json object to php associative array
    $list = json_decode($jsondata, true);
    for ($x = 0; $x <= count($list); $x++) {
        $curRow   = $list[$x];
        $username = $curRow["username"];
        echo "<h3> $x)The name is:   $username  </h3>";
        
        if (empty($username)) {
            return;
        }
        //check duplicate 
        $selectQuery    = 'SELECT id  FROM  ' . $GLOBALS['developertable'];
        $whereStatement = " where 1=1   ";
        
        $ref = $curRow["ref"];
        if (!empty($ref)) {
            $whereStatement = $whereStatement . " and source = '" . $ref . "' ";
        }
        
        if (!empty($username)) {
            $whereStatement = $whereStatement . " and username = '" . $username . "' ";
        }
        echo " <br/> $selectQuery . $whereStatement ";
        $result         = $db->query($selectQuery . $whereStatement);
        $curdate        = date('Y-m-d H:i:s');
        $ref            = safeText($db->real_escape_string($curRow["ref"]));
        $fullname       = safeText($db->real_escape_string($curRow["fullname"]));
        $company        = safeText($db->real_escape_string($curRow["company"]));
        $fullname       = safeText($db->real_escape_string($curRow["fullname"]));
        $email          = safeText($db->real_escape_string($curRow["email"]));
        $location       = safeText($db->real_escape_string($curRow["location"]));
        $specialist     = safeText($db->real_escape_string($curRow["specialist"]));
        $totalFollowing = safeText($db->real_escape_string($curRow["totalFollowing"]));
        $totalFollowers = safeText($db->real_escape_string($curRow["totalFollowers"]));
        $totalStars     = safeText($db->real_escape_string($curRow["totalStars"]));
        $totalRepo      = safeText($db->real_escape_string($curRow["totalRepo"]));
         
        if ($result->num_rows > 0) {
            
            echo "<h1>Skipped insertion. Duplicate record exists for [ref//username] = " . $ref . "//" . $username . "</h1>";
        } else {
            //insert json into sql  
            $statement    = "INSERT INTO " . $GLOBALS['developertable'] . "(source, fullname, username, company, email, location, specialist, totalFollowing, totalFollowers, totalStars ,totalRepo, createdby,createddate)
                VALUES('$ref', '$fullname', '$username', '$company','$email', '$location','$specialist', '$totalFollowing','$totalFollowers','$totalStars','$totalRepo','*',  '$curdate')";
            $resultInsert = crudDB($statement);
            if (!$resultInsert) {
                echo "<h3>" . $statement . "</h3>";
                echo " <h1>Performed succcess insertion for [ref//username] = " . $ref . "//" . $username . " >insert_id=" . $resultInsert->insert_id . "</h1>";
            }
        }
        
    }
}

function updateGithubUsersContributions($app)
{
    
    $db = connect_db();
    
    //read json files
    //loop records
    //read the json file contents
   // $jsondata = file_get_contents('../shared/UserContribution-Github.txt');
    // var_dump($jsondata  ) ;  
    
$myfile = fopen('../shared/UserContribution-Github.txt', "r") or die("Unable to open file!");
$jsondata = fread($myfile,filesize('../shared/UserContribution-Github.txt'));
fclose($myfile);

    //convert json object to php associative array
    $list = json_decode($jsondata, true);
    for ($x = 0; $x <= count($list); $x++) {
        $curRow         = $list[$x];
        $username       = $db->real_escape_string($curRow["username"]);
        $curdate        = date('Y-m-d H:i:s');
        $ref            = $db->real_escape_string($curRow["ref"]);
        $fullname       = $db->real_escape_string($curRow["fullname"]);
        $company        = $db->real_escape_string($curRow["company"]);
        $fullname       = $db->real_escape_string($curRow["fullname"]);
        $email          = $db->real_escape_string($curRow["email"]);
        $location       = $db->real_escape_string($curRow["location"]);
        $specialist     = $db->real_escape_string($curRow["specialist"]);
        $totalFollowing = $db->real_escape_string($curRow["totalFollowing"]);
        $totalFollowers = $db->real_escape_string($curRow["totalFollowers"]);
        $totalStars     = $db->real_escape_string($curRow["totalStars"]);
        $totalRepo      = $db->real_escape_string($curRow["totalRepo"]);
        $year           = $db->real_escape_string($curRow["year"]);
        $contribution   = $db->real_escape_string($curRow["contribution"]);
        echo "<h3><br> $x)The name is:   $username  </h3>";
        echo "<h3>  The year is:   $year  </h3>";
        echo "<h3>  The contribution is:   $contribution  </h3>";
        
        if (empty($username)) {
            return;
        }
        
        //get dev id
        $selectQuery    = 'SELECT distinct id AS developerid FROM  ' . $GLOBALS['developertable'];
        $whereStatement = " where 1=1   ";
        
        
        if (!empty($username)) {
            $whereStatement = $whereStatement . " and username = '" . $username . "' ";
        }
        
        $result      = queryDB($selectQuery . $whereStatement);
        $developerid = $result[0]["developerid"];
        
        //check duplicate 
        $selectQuery    = 'SELECT distinct id  FROM  ' . $GLOBALS['developercontributiontable'];
        $whereStatement = " where 1=1   ";
        
        if (!empty($developerid)) {
            $whereStatement = $whereStatement . " and developerid = '" . $developerid . "' ";
        }
        
        if (!empty($year)) {
            $whereStatement = $whereStatement . " and year = '" . $year . "' ";
        }
        $result = queryDB($selectQuery . $whereStatement);
        
        if (count($result) > 0) {
            echo "<h1>Skipped insertion. Developer contribution already exist [ref//username//year] = " . $ref . "//" . $username . "//" . $year . "</h1>";
        } else {
            $statement = "INSERT INTO " . $GLOBALS['developercontributiontable'] . "(developerid, year, contributiondates,   createdby,createddate)
            VALUES('$developerid'  ,'$year',  '$contribution','*',  '$curdate')";
            
            $resultInsert = crudDB($statement);
            
            if ($resultInsert) {
                echo "<h3>" . $statement . "</h3>";
                echo " <h1>Performed succcess insertion for [ref//username] = " . $ref . "//" . $username . " >insert_id=" . $resultInsert->insert_id . "</h1>";
            }
        }
        
    }
    
}

function listSlideShareUsers($app)
{
    $mode = $app->request()->get('mode');
    
    $db             = connect_db();
    $statement      = 'SELECT dev.source ,dev.username , dev.fullname ,dev.company ,dev.web,dev.facebook,dev.linkedin,dev.twitter,dev.specialist,dev.location ,dev.totalDocument , dev.totalInfographics , dev.totalpresentationslides , dev.totalvideo , ss.slidelinks as slidelinks ,ss.slidecontent as slidecontents ,ss.createddate as processeddate ,dev.id as developerid , dev.username FROM  ' . $GLOBALS['developertable'] . ' dev , ' . $GLOBALS['developerslidetable'] . ' ss ';
    $wherestatement = '  where 1=1 and dev.id = ss.developerid';
    // var_dump( $statement . $wherestatement ) ; 
    $result         = queryDB($statement . $wherestatement);
    $data           = array();
    // var_dump(  count($result)) ; 
    if (count($result) > 0) {
        $data = $result;
        
    } else {
        echo "<h1>NO RECORD. Please run http://url?mode=UPDATE-SLIDESHARE </h1>";
    }
    $app->render('listslideshare.php', array(
        'page_title' => "Your List",
        'data' => $data
    ));
}

function listGithubUsers($app)
{
    $mode = $app->request()->get('mode');
    
    $db             = connect_db();
    $statement      = 'SELECT dev.source ,dev.username , dev.fullname ,dev.company ,dev.totalStars,dev.totalFollowers,dev.totalRepo,dev.totalFollowing,dev.specialist,dev.location ,dev.email , devcon.year as year ,devcon.contributiondates as contributiondates ,devcon.createddate as processeddate ,dev.id as developerid , dev.username FROM  ' . $GLOBALS['developertable'] . ' dev , ' . $GLOBALS['developercontributiontable'] . ' devcon ';
    $wherestatement = '  where 1=1 and dev.id = devcon.developerid';
    // var_dump( $statement . $wherestatement ) ; 
    $result         = queryDB($statement . $wherestatement);
    $data           = array();
    // var_dump(  count($result)) ; 
    if (count($result) > 0) {
        $data = $result;
        
    } else {
        echo "<h1>NO RECORD. Please run http://url?mode=UPDATE-GITHUB </h1>";
    }
    $app->render('list.php', array(
        'page_title' => "Your List",
        'data' => $data
    ));
}
?> 

 