 
<!doctype html>
<html>
<head>

<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="./templates/bootstrap/bootstrap.css">
<meta charset="utf-8">
<title><?php echo $this->data['page_title']; ?></title>
</head>
<body> 

<div class="container">
  <h2>Result Set</h2>
                                                                                       
  <div class="table-responsivex">          
  <table class="table">
    <thead>
      <tr> 
        <th>Source</th>
        <th>Developer Id</th>
        <th>Full Name</th>
        <th>Username</th>
        <th>Email</th>
        <th>Company</th>
        <th>Location</th>
        <th>Specialist</th>
        <th>Total Following</th>
        <th>Total Followers</th>
        <th>Total Stars</th>
        <th>Total Repo</th>
        <th>Year</th>
        <th>Contributions</th>
        <th>Processed Date</th>
      </tr>
    </thead>
    <tbody>
     
		<?php
		foreach ($this->data['data'] as $row) {
		//	echo $row['id'].' - '.$row['username'].' - '.$row['fullname'].'</br />';
			echo " <tr> " ;
			echo ' <td>' . $row["source"]. '</td>';	
			echo ' <td>' . $row["developerid"]. '</td>';
			echo ' <td>' . $row["fullname"]. '</td>';
			echo ' <td>' . $row["username"]. '</td>';
			echo ' <td>' . $row["email"]. '</td>';
			echo ' <td>' . $row["company"]. '</td>';
			echo ' <td>' . $row["location"]. '</td>';
			echo ' <td>' . $row["specialist"]. '</td>';
			echo ' <td>' . $row["totalFollowing"]. '</td>';
			echo ' <td>' . $row["totalFollowers"]. '</td>';
			echo ' <td>' . $row["totalStars"]. '</td>';
			echo ' <td>' . $row["totalRepo"]. '</td>';
			echo ' <td>' . $row["year"]. '</td>';
			echo ' <td>' . $row["contributiondates"]. '</td>';
			echo ' <td>' . $row["processeddate"]. '</td>';
			echo " </tr> " ;
		}


 
		?>    
      
       
    </tbody>
  </table>
  </div>
</div>

<script src="./templates/bootstrap/jquery.js"></script>
<script src="./templates/bootstrap/bootstrap.js"></script>
  
</body>
</html>
 