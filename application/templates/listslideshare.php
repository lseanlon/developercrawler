 
<!doctype html>
<html>
<head>

<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="./templates/bootstrap/bootstrap.css">
<meta charset="utf-8">
<title><?php echo $this->data['page_title']; ?></title>
</head>
<body> 

<div class="container">
  <h2>Result Set</h2>
                                                                                       
  <div class="table-responsivex">          
  <table class="table">
    <thead>
      <tr> 
        <th>Source</th>
        <th>Developer Id</th>
        <th>Full Name</th>
        <th>Username</th>  
        <th>Location</th>
        <th>Specialist</th>
        <th>Facebook</th>
        <th>LinkedIn</th>
        <th>Twitter</th>
        <th>Total Documents</th>
        <th>Total Infographics</th>
        <th>Total Slides</th>
        <th>Total Videos</th>
        <th>Slide Links</th>
        <th>Slide Contents</th>
        <th>Processed Date</th>
      </tr>
    </thead>
    <tbody> 
		<?php
		foreach ($this->data['data'] as $row) {
		//	echo $row['id'].' - '.$row['username'].' - '.$row['fullname'].'</br />';
			echo " <tr> " ;
			echo ' <td>' . $row["source"]. '</td>';	
			echo ' <td>' . $row["developerid"]. '</td>';
			echo ' <td>' . $row["fullname"]. '</td>';
			echo ' <td>' . $row["username"]. '</td>';  
			echo ' <td>' . $row["location"]. '</td>';
			echo ' <td>' . $row["specialist"]. '</td>';
      echo ' <td>' . $row["facebook"]. '</td>';
      echo ' <td>' . $row["linkedin"]. '</td>';
      echo ' <td>' . $row["twitter"]. '</td>';
			echo ' <td>' . $row["totalDocument"]. '</td>';
			echo ' <td>' . $row["totalInfographics"]. '</td>';
			echo ' <td>' . $row["totalpresentationslides"]. '</td>';
			echo ' <td>' . $row["totalvideo"]. '</td>';
			echo ' <td>' . $row["slidelinks"]. '</td>';
			echo ' <td>' . $row["slidecontents"]. '</td>';
			echo ' <td>' . $row["processeddate"]. '</td>';
			echo " </tr> " ;
		}


 
		?>    
      
       
    </tbody>
  </table>
  </div>
</div>

<script src="./templates/bootstrap/jquery.js"></script>
<script src="./templates/bootstrap/bootstrap.js"></script>
  
</body>
</html>
 