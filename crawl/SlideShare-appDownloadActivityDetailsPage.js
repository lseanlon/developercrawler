var cheerio = require('cheerio');
var fs = require('fs');
var download = require('download');
var mkdirp = require('mkdirp');
require('shelljs/global');


var getUrlEntry = function() {
    var paramValue = './src/configuration/UrlList.txt';
    process.argv.forEach(function(val, index, array) {

        // node app  src/UrlList.txt 
        // eg. [0] node / [1] app/ [2]'src/UrlListInfo.txt'
        //console.log(index + ': ' + val);  

        //map file name
        if (index == 2 && val) {
            paramValue = val;
        }

    });

    return paramValue;
};

var urlEntry = fs.readFileSync(getUrlEntry(), 'utf8');
urlList = urlEntry.split("\n");
var outputJson = {};

//YEAR PARAM  
Date.prototype.yearr = function() {
    var mm = this.getMonth() + 1; // getMonth() is zero-based
    var dd = this.getDate();

    return this.getFullYear();
};
//default
var todayDate = new Date();
var currentYear = todayDate.yearr();
var MIN_YEAR = currentYear - 7;

//file parameter
var yearEntry = fs.readFileSync( './src/configuration/YearInfo.txt', 'utf8');
yearEntryList = yearEntry.split("-");
MIN_YEAR = yearEntryList[0].replace('[THISYEAR]', todayDate.yearr());
currentYear = yearEntryList[1].replace('[THISYEAR]', todayDate.yearr());




//timer in second
var timerWait = fs.readFileSync('./src/configuration/TimerInfo.txt', 'utf8');
timerWait = parseInt(timerWait) * 1000;


function exitHandler(options, err) {
    if (options.cleanup) console.log('clean');
    if (err) console.log(err.stack);
    if (options.exit) process.exit();
}

var listOfLink = [];
//download all the listing links
urlList.forEach(function(_elem, index, collection) {
    var urlInfo = _elem.split("|");
    var urlPath = urlInfo[0];
    var urlLink = urlInfo[1];
    var ref = urlInfo[2];


    var filepath = './dist/SlideShareRecordFinalDetails-' + ref + '.txt'
    var jsonListIndexStr = fs.readFileSync(filepath, 'utf8');
    var jsonIndexList = JSON.parse(jsonListIndexStr);

    var randomNum = Math.floor(Math.random() * 574) + 1;
    var list = jsonIndexList;
    for (var i = 1; i <= list.length; i++) {

        downloadAllLinkPage(i, currentYear, list[i], timerWait + randomNum, (i + 1) == list.length)
    }


});
Date.prototype.yyyymmdd = function() {
    var mm = this.getMonth() + 1; // getMonth() is zero-based
    var dd = this.getDate();

    return [this.getFullYear(),
        (mm > 9 ? '' : '0') + mm,
        (dd > 9 ? '' : '0') + dd
    ].join('-');
};

function downloadAllLinkPage(i, _currentYear, _elem, timerWait, _isLastIndex) {
    var extraTimer = 0;

    if (!_elem || _currentYear < MIN_YEAR) {
        return;
    }

    var username = _elem.username;

    console.log('_currentYear', _currentYear);
    console.log('username', username);
    var outputfolderpath = "./dist/FinalUserPage/" + username + "/";

    var urlLinkPage = "https://github.com/" + username + "?tab=overview&from=" + _currentYear + "-12-01&to=" + _currentYear + "-12-31&utf8=%E2%9C%93"
        // https://github.com/username?tab=overview&from=2013-12-01&to=2013-12-31&utf8=%E2%9C%93


    fs.stat(outputfolderpath, function(err, stats) {
        if (err) {
            mkdirp(outputfolderpath, function(err) {
                if (err) return cb(err);
                console.log("create folder path", outputfolderpath);
            });
        }

    });



    var outputfilepath = outputfolderpath + _currentYear + ".txt";
    _elem.year = _currentYear;
    _elem['urlLinkPage'] = urlLinkPage
    _elem['outputfilepath'] = outputfilepath

    // if (!listOfLink[_elem.ref]) {
    //     listOfLink[_elem.ref] = [];
    // }
    // listOfLink[_elem.ref].push(_elem);
    listOfLink.push(_elem);
    _isLastIndex = true;

    if (_isLastIndex) {}
    var pathname = "./dist/downloadedFinalUserDetails-" + _elem.ref + ".txt"
    fs.writeFileSync(pathname, JSON.stringify(listOfLink));
    console.log("Write download result to path ", pathname);


    if (fs.existsSync(outputfilepath)) {
        extraTimer = 0;
    } else {
        extraTimer = 2650;
    }

    var finalTimer = i * extraTimer
    if(i>60 || finalTimer>130000){
        finalTimer = 60 * extraTimer
        finalTimer= 5.556 *1000;
    }
    setTimeout(function() {


        if (fs.existsSync(outputfilepath)) {
            console.log("File downloaded. Already found. Skipped downloading ", urlLinkPage);
            console.log("File downloaded. Skipped downloading path ", outputfilepath);

        } else {


            console.log("start downloading ", urlLinkPage);



            // var sleepTimer = " sleep " +(i*0.334)+ "s ";
            // var sleepTimerEnd = " sleep " +(i*0.34)+ "s ";


            var sleepTimer = " sleep " + (1.453) + "s ";
            var sleepTimerEnd = " sleep " + (1.111) + "s ";
            var commandRun = sleepTimer + '&& phantomjs PageDownload6.js "' + (urlLinkPage) + '" &&' + sleepTimerEnd;
            try {

                exec(commandRun, function(status, output) {
                    console.log('Exit status:', status);
                    console.log('Program output:');

                    if (!urlLinkPage || !output) {
                        return;
                    }

                    fs.writeFileSync(outputfilepath, output);
                    console.log('done download - ' + urlLinkPage + ' into ' + outputfilepath);

                });

            } catch (e) {
                console.log(e);

            }

        }



        downloadAllLinkPage(i, _currentYear - 1, _elem, timerWait, _isLastIndex);

    }, (i * finalTimer));



}
