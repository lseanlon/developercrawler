var cheerio = require('cheerio');
var fs = require('fs');
var download = require('download');
var mkdirp = require('mkdirp');

require('shelljs/global');
var downloadPath = fs.readFileSync('./src/configuration/EntryPathInfo.txt', 'utf8');
var maxScroll = fs.readFileSync('./src/configuration/SlideShareMaxPaginationScroll.txt', 'utf8');
var outputJson = {};
var getUrlEntry = function() {

    var paramValue = './src/configuration/SlideShareUrlList.txt';
    process.argv.forEach(function(val, index, array) {

        //map file name
        if (index == 2 && val) {
            paramValue = val;
        }

    });

    return paramValue;
};

var urlEntry = fs.readFileSync(getUrlEntry(), 'utf8');
urlList = urlEntry.split("\n");

//timer in second
var timerWait = fs.readFileSync('./src/configuration/TimerInfo.txt', 'utf8');
timerWait = parseInt(timerWait) * 1000;

Date.prototype.yyyymmdd = function() {
    var mm = this.getMonth() + 1; // getMonth() is zero-based
    var dd = this.getDate();

    return [this.getFullYear(),
        (mm > 9 ? '' : '0') + mm,
        (dd > 9 ? '' : '0') + dd
    ].join('-');
};

var todayDate = new Date();
todayDate = todayDate.yyyymmdd();

var downloadedEntryPathList = [];
//download all the first page listing
console.log('urlList' , urlList);
urlList.forEach(function(_elem, index, collection) {
    if (!_elem) {
        return;
    }

    var urlInfo = _elem.split("|");
    var urlPath = urlInfo[0];
    var urlLink = urlInfo[1].replace('[TODAY_DATE]', todayDate);
    var ref = urlInfo[2];

    var curRow = { 'ref': ref, 'urlLink': urlLink, 'urlPath': urlPath, 'downloadPath': downloadPath };
    console.log('  curRow', curRow);

    //add random number to timer
    var randomNum = Math.floor(Math.random() * 574) + 1;

    if (fs.existsSync(urlPath)) {
        randomNum = -3000;
    }
    var finaltimer = index * timerWait + randomNum
    if (finaltimer>60000) {
        finaltimer = 7.454 *1000;
    }
    if (fs.existsSync(urlPath)) {
        finaltimer = -20;
    }

    mkdirp("./dist/EntryPage", function(err) {
        if (err) return cb(err);
        console.log("create folder path", "./dist/EntryPage");
    });


    //add timeout - reduce suspicious of crawling
    setTimeout(function() {

        console.log('Check exist ');
        // if ( fs.existsSync(urlPath)) {
        //     console.log('File downloaded found.Skipping', urlPath);
        // } else {  }

        console.log('start download ', urlLink);

        var commandRun = 'sleep 1.1s && phantomjs PageDownloadScroll.js "' + (urlLink) + '" "'+  maxScroll +  '" ';
        try {

            exec(commandRun, function(status, output) {
                console.log('Exit status:', status);
                console.log('Program output:');

                console.log('done download urlLink', urlLink);
                console.log('done download urlPath', urlPath);



                if (!urlPath || !output) {
                    return;
                }

                fs.writeFileSync(urlPath, output);
                 handleSubPages(curRow);


            });

        } catch (e) {
            console.log(e);

        }
        downloadedEntryPathList.push(curRow);



    }, finaltimer);

});

var handleSubPages = function(_elem) {


    console.log('handleSubPages');
    //each first page lisitng, download al the sub pages 
    var randomNum = null;

    //add random number to timer
    randomNum = Math.floor(Math.random() * 574) + 1;


    var fileContent = fs.readFileSync(_elem.urlPath, 'utf8');

    var $ = cheerio.load(fileContent);


    //divide no of record over pagination, use ceiling
    var numberOfPage = maxScroll 
    console.log('numberOfPage', numberOfPage);

    // determine record per page
    var listingPerPage = 6; 
    console.log('listingPerPage', listingPerPage);  


   //var filePathFormat = _elem.downloadPath + "pages-" + _elem.ref;
    var filePathFormat =  _elem.urlPath;

    outputJson = { "numberOfPage": numberOfPage, "ref": _elem.ref, "filePathFormat": filePathFormat };
    fs.appendFileSync('./dist/SlideShareListingIndex.txt', JSON.stringify(outputJson) + ",");
  

};

function removeURLParameter(url, parameter) {
    //prefer to use l.search if you have a location/link object
    var urlparts = url.split('?');
    if (urlparts.length >= 2) {

        var prefix = encodeURIComponent(parameter) + '=';
        var pars = urlparts[1].split(/[&;]/g);

        //reverse iteration as may be destructive
        for (var i = pars.length; i-- > 0;) {
            //idiom for string.startsWith
            if (pars[i].lastIndexOf(prefix, 0) !== -1) {
                pars.splice(i, 1);
            }
        }

        url = urlparts[0] + '?' + pars.join('&');
        return url;
    } else {
        return url;
    }
}
 
