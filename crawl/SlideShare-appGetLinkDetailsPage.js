var cheerio = require('cheerio');
var fs = require('fs');
var download = require('download');

var jsonListIndexStr = "[" + fs.readFileSync("./dist/SlideShareListingIndex.txt", 'utf8') + "{}]";
var jsonIndexList = JSON.parse(jsonListIndexStr);

var downloadPath = fs.readFileSync('./src/configuration/EntryPathInfo.txt', 'utf8');
var outputJson = {};


//timer in second
var timerWait = fs.readFileSync('./src/configuration/TimerInfo.txt', 'utf8');
timerWait = parseInt(timerWait) * 1000;

//get config
//loop config, 
// loop indexes

var listOfLink = {}

var downloadedEntryPathList = [];
//download all the first page listing
jsonIndexList.forEach(function(_elem, index, collection) {

    var randomNum = Math.floor(Math.random() * 574) + 1;

    if (!_elem || !_elem.numberOfPage) {
        return;
    }

    //all pages loaded into 1 file instead
    _elem.numberOfPage = 1;
    for (var i = 1; i <= _elem.numberOfPage; i++) {
        addAllLinkInformation(i, _elem, timerWait + randomNum, i == _elem.numberOfPage);
    }

});




function addAllLinkInformation(i, _elem, timerWait, _isLastIndex) {
    // grab file, cheerio
    // grab link html

    if (!_elem) {
        return;
    }


    var fileContent = '';
    var filefullnamepath = _elem.filePathFormat;
    try {
        fileContent = fs.readFileSync(filefullnamepath, 'utf8');
    } catch (e) {
        console.log('SKIPPED READ ,BECAUSE DOWNLOAD FAILED FOR ', filefullnamepath);
        return;
    }


    var $ = cheerio.load(fileContent)


    if (!listOfLink[_elem.ref]) { listOfLink[_elem.ref] = []; }
    var temList = [];

    $('li.iso_slideshow').each(function(i, element) {

        var curLink = 'https://www.slideshare.net' + $(this).find('.thumbnail-content .details .title.title-link').prop('href');
        var curSlideTitle = $(this).find('.thumbnail-content .details a').html();
        var curSlideAuthor = $(this).find('.thumbnail-content .details .author a').html();
        var curSlideInfo = $(this).find('.thumbnail-content .details .info').html();
        var curSlideAuthorLink = 'http://www.slideshare.net' + $(this).find('.thumbnail-content .details .author a').prop('href');
        var curRow = { "link": curSlideAuthorLink, "ref": _elem.ref ? _elem.ref : "" };
        curRow['curSlideTitle'] = curSlideTitle;
        curRow['curSlideAuthor'] = curSlideAuthor;
        curRow['curSlideAuthorLink'] = curSlideAuthorLink;
        curRow['curSlideInfo'] = curSlideInfo;

        var strList = JSON.stringify(listOfLink[_elem.ref]);
        if (strList.indexOf(curSlideTitle) == -1) {
            listOfLink[_elem.ref].push(curRow);
        }
    });



    if (_isLastIndex) {}
    var filepath = "./dist/SlideShareDetailLinkIndex.txt"
    fs.writeFileSync(filepath, JSON.stringify(listOfLink));
    console.log('written to file', filepath);


}
