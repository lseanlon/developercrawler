var cheerio = require('cheerio');
var fs = require('fs');
var download = require('download');


var downloadPath = fs.readFileSync('./src/configuration/EntryPathInfo.txt', 'utf8');
var outputJson = {};
var getUrlEntry = function() {

    var paramValue = './src/configuration/UrlList.txt';
    process.argv.forEach(function(val, index, array) {

        // node app  src/UrlList.txt 
        // eg. [0] node / [1] app/ [2]'src/UrlListInfo.txt'
        //console.log(index + ': ' + val);  

        //map file name
        if (index == 2 && val) {
            paramValue = val;
        }

    });

    return paramValue;
};

// read downloadedFinalDetails file
// parse json
// loop entire folder, read entire index file
// scrap out all row into json
// save into output file

var urlEntry = fs.readFileSync(getUrlEntry(), 'utf8');
urlList = urlEntry.split("\n");

//timer in second
var timerWait = fs.readFileSync('./src/configuration/TimerInfo.txt', 'utf8');
timerWait = parseInt(timerWait) * 1000;

var striphtml = function(html) {
    var tmp = document.createElement("DIV");
    tmp.innerHTML = html;
    return tmp.textContent || tmp.innerText || "";
}
var chunks = require('chunk-array').chunks
var finalResultList = [];
var finalResultExcelList = [];
var downloadedEntryPathList = [];
//download all the first page listing
var ref = '';

try {

    urlList.forEach(function(_elem, index, collection) {
        var urlInfo = _elem.split("|");
        ref = urlInfo[2];

        var jsonFileDetails;
        var filefullnamepath = './dist/SlideShareDownloadedFinalDetails-' + ref + '.txt'
        try {
            jsonFileDetails = fs.readFileSync(filefullnamepath, 'utf8');
            jsonFileDetails = JSON.parse(jsonFileDetails);

        } catch (e) {
            console.log('SKIPPED READ BECAUSE DOWNLOAD FAILED FOR ', filefullnamepath);
            return;
        }

        var listRef = jsonFileDetails[ref];
        for (var i = 0; i <= listRef.length; i++) {
            if (listRef[i]) {

                var elem = listRef[i];
                console.log("Reading file ", elem["outputfilepath"]);
                var filehtml = fs.readFileSync(elem["outputfilepath"], 'utf8');
                var $ = cheerio.load(filehtml);

                var currentRow = listRef[i];


                var sanitizeDash = function(_val) {
                    //take out all html   
                    return ($("<p>" + _val + "</p>").text());
                }


                currentRow['ref'] = ref;
                //contactName
                if ($("[itemprop='name']")) {
                    currentRow['name'] = sanitizeDash($("[itemprop='name']").html())
                }
                //image
                if ($("[itemprop='image']")) {
                    currentRow['image'] = sanitizeDash($("[itemprop='image']").prop('src'))
                }
                if ($(".location")) {
                    currentRow['location'] = sanitizeDash($(".location").html())
                }

                if ($(".spriteProfile.industry")) {
                    currentRow['industry'] = sanitizeDash($(".spriteProfile.industry").html())
                }
                if ($(".spriteProfile.web")) {
                    currentRow['web'] = sanitizeDash($(".spriteProfile.web a").html())
                }
                if ($(".spriteProfile.facebook")) {
                    currentRow['facebook'] = sanitizeDash($(".spriteProfile.facebook").prop('href'))
                }
                if ($(".spriteProfile.linkedin")) {
                    currentRow['linkedin'] = sanitizeDash($(".spriteProfile.linkedin").prop('href'))
                }
                if ($(".spriteProfile.twitter")) {
                    currentRow['twitter'] = sanitizeDash($(".spriteProfile.twitter").prop('href'))
                }
                if ($(".j-docCount.accordionCount")) {
                    currentRow['totalDocument'] = sanitizeDash($(".j-docCount.accordionCount").html())
                }
                if ($(".j-infogrCount.accordionCount")) {
                    currentRow['totalInfographics'] = sanitizeDash($(".j-infogrCount.accordionCount").html())
                }
                if ($(".j-pptCount.accordionCount")) {
                    currentRow['totalPresentationSlides'] = sanitizeDash($(".j-pptCount.accordionCount").html())
                }
                if ($(".j-vidCount.accordionCount")) {
                    currentRow['totalVideo'] = sanitizeDash($(".j-vidCount.accordionCount").html())
                }


                currentRow['presentationSlidesContent'] = "";
                currentRow['presentationSlidesLink'] = "";
                $("[id^=presentation]").each(function(iddxx, elemm) {
                    if ($(this).find('a')) {
                        var $content = $(this).find('a');
                        var contentt = $content.prop('title') + ' [' + $content.prop('data-hits') + ']';
                        currentRow['presentationSlidesContent'] = currentRow['presentationSlidesContent'] + " / " + contentt
                        var linkk = 'https://slideshare.net' + sanitizeDash($content.prop('href'));
                        currentRow['presentationSlidesLink'] = currentRow['presentationSlidesLink'] + " | " + linkk
                    }

                });




                var filepath = './dist/SlideShareRecordFinalDetails-' + ref + '.txt'
                console.log("Write to file ", filepath);
                fs.writeFileSync(filepath, JSON.stringify(finalResultList));


                var filepath = '../shared/SlideShareUser-' + ref + '.txt'
                console.log("Write to file ", filepath);
                fs.writeFileSync(filepath, JSON.stringify(finalResultList));

                console.log("currentRow ", currentRow);
                if (!finalResultExcelList[ref]) { finalResultExcelList[ref] = []; }
                finalResultList.push(currentRow);
                finalResultExcelList[ref].push(currentRow);
            }


        }

        if (urlList.length == (index + 1)) {}


    });


} catch (e) {
    console.log(e);
}


//write into excel 
var json2xls = require('json2xls');
for (var key in finalResultExcelList) {
    if (finalResultExcelList.hasOwnProperty(key)) {
        // console.log(key + " -> " + finalResultExcelList[key]);  
        var xls = json2xls(finalResultExcelList[key]);
        var outexcelpath = './dist/SlideShare' + key + '-data.xlsx';
        console.log("Write to excel ", outexcelpath);
        fs.writeFileSync(outexcelpath, xls, 'binary');
    }
}
