var cheerio = require('cheerio');
var fs = require('fs');
var download = require('download');
// var sleeper = require('sleep');
var generalUrlHeader =  '-H "Pragma: no-cache" -H "Accept-Encoding: gzip, deflate, sdch, br" -H "Accept-Language: en-US,en;q=0.8" -H "Upgrade-Insecure-Requests: 1" -H "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.1883.87 Safari/537.36" -H "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8" -H "Cache-Control: no-cache" -H "Cookie: logged_in=no; _octo=GH1.1.1831883623.1486184331; _gh_sess=; _ga=GA1.2.291845938.1486184330; _gat=1; tz=Asia%2FKuala_Lumpur" -H "Connection: keep-alive" --compressed'
 
var mkdirp = require('mkdirp');
require('shelljs/global');


var getUrlEntry = function() {
    var paramValue = './src/configuration/UrlList.txt';
    process.argv.forEach(function(val, index, array) {


        //map file name
        if (index == 2 && val) {
            paramValue = val;
        }

    });

    return paramValue;
};

var urlEntry = fs.readFileSync(getUrlEntry(), 'utf8');
urlList = urlEntry.split("\n");
var outputJson = {};

//YEAR PARAM  
Date.prototype.yearr = function() {
    var mm = this.getMonth() + 1; // getMonth() is zero-based
    var dd = this.getDate();

    return this.getFullYear();
};
//default
var todayDate = new Date();
var currentYear = todayDate.yearr();
var MIN_YEAR = currentYear - 7;

//file parameter
var yearEntry = fs.readFileSync('./src/configuration/YearInfo.txt', 'utf8');
yearEntryList = yearEntry.split("-");
MIN_YEAR = yearEntryList[0].replace('[THISYEAR]', todayDate.yearr());
currentYear = yearEntryList[1].replace('[THISYEAR]', todayDate.yearr());




//timer in second
var timerWait = fs.readFileSync('./src/configuration/TimerInfo.txt', 'utf8');
timerWait = parseInt(timerWait) * 1000;


function exitHandler(options, err) {
    if (options.cleanup) console.log('clean');
    if (err) console.log(err.stack);
    if (options.exit) process.exit();
}

var listOfLink = [];
//download all the listing links
urlList.forEach(function(_elem, index, collection) {
    var urlInfo = _elem.split("|");
    var urlPath = urlInfo[0];
    var urlLink = urlInfo[1];
    var ref = urlInfo[2];


    var filepath = './dist/recordFinalDetails-' + ref + '.txt'
    var jsonListIndexStr = fs.readFileSync(filepath, 'utf8');
    var jsonIndexList = JSON.parse(jsonListIndexStr);

    var randomNum = Math.floor(Math.random() * 574) + 1;
    var list = jsonIndexList;
    for (var i = 0; i <= list.length; i++) {
        if (list[i]) {
            downloadAllLinkPage(i, currentYear, list[i], timerWait + randomNum, (i) == list.length)
        }
    }


});
Date.prototype.yyyymmdd = function() {
    var mm = this.getMonth() + 1; // getMonth() is zero-based
    var dd = this.getDate();

    return [this.getFullYear(),
        (mm > 9 ? '' : '0') + mm,
        (dd > 9 ? '' : '0') + dd
    ].join('-');
};

function downloadAllLinkPage(i, _currentYear, _elem, timerWait, _isLastIndex) {
    var extraTimer = 0;

    if (!_elem || _currentYear < MIN_YEAR) {
        return;
    }

    var username = _elem.username;

    console.log('i', i);
    console.log('currentYear', _currentYear);
    console.log('username', username);
    var outputfolderpath = "./dist/FinalUserPage/" + username + "/";

    var urlLinkPage = "https://github.com/" + username + "?tab=overview&from=" + _currentYear + "-12-01&to=" + _currentYear + "-12-31&utf8=%E2%9C%93"
        // https://github.com/username?tab=overview&from=2013-12-01&to=2013-12-31&utf8=%E2%9C%93

    fs.stat(outputfolderpath, function(err, stats) {
        if (err) {
            mkdirp(outputfolderpath, function(err) {
                if (err) return cb(err);
                console.log("create folder path", outputfolderpath);
            });
        }

    });



    var outputfilepath = outputfolderpath + _currentYear + ".txt";
    _elem.year = _currentYear;
    _elem['urlLinkPage'] = urlLinkPage
    _elem['outputfilepath'] = outputfilepath

    // if (!listOfLink[_elem.ref]) {
    //     listOfLink[_elem.ref] = [];
    // }
    // listOfLink[_elem.ref].push(_elem);
    listOfLink.push(_elem);
    if (_isLastIndex) {}
    var pathname = "./dist/downloadedFinalUserDetails-" + _elem.ref + ".txt"
    fs.writeFileSync(pathname, JSON.stringify(listOfLink));
    console.log("Write download result to path ", pathname);


    if (fs.existsSync(outputfilepath)) {
        extraTimer = 0;
    } else {
        extraTimer = 2650;
    }

    var finalTimer = i * extraTimer
    if (i > 60 || finalTimer > 125000) {
        finalTimer = 60 * extraTimer
        finalTimer = 92.3556;
        finalTimer = 1.3556 * 1000;
    }
    setTimeout(function() {

            if (fs.existsSync(outputfilepath)) {
                console.log("File downloaded. Already found. Skipped downloading ", urlLinkPage);
                console.log("File downloaded. Skipped downloading path ", outputfilepath);

            } else {

                console.log("start downloading ", urlLinkPage);

              //  sleeper.sleep(1);
                var timerval = (i * 3.95) > 30 ? 13.52 : (i * 3.35);
                var sleepTimer = " sleep " + timerval + "s ";
                var sleepTimerEnd = " sleep " + timerval + "s ";
                try {
                    var commandRun = sleepTimer + '  && curl "' + (urlLinkPage) +  '" '+ generalUrlHeader +' > "' + outputfilepath + '" &&   ' + sleepTimerEnd;
  
                    exec(commandRun, function(status, output) {
                        console.log('done download - ' + urlLinkPage + ' into ' + outputfilepath);

                    });

                } catch (e) {
                    console.log(e);

                } 

                    // var sleepTimer = " sleep " + (3.453) + "s ";
                    // var sleepTimerEnd = " sleep " + (4.111) + "s ";
                    // var commandRun = sleepTimer + '&& phantomjs PageDownload6.js "' + (urlLinkPage) + '" &&' + sleepTimerEnd;
                    // try {d

                    //     console.log("start downloading ", urlLinkPage);

                    //     exec(commandRun, function(status, output) {
                    //         console.log('Exit status:', status);
                    //         console.log('Program output:');

                    //         if (!urlLinkPage || !output) {
                    //             return;
                    //         }

                    //         fs.writeFileSync(outputfilepath, output);
                    //         console.log('done download - ' + urlLinkPage + ' into ' + outputfilepath);

                    //     });

                    // } catch (e) {
                    //     console.log(e);

                    // }

                }

                if (_isLastIndex) {
                    console.log('PROGRAM ENDS');
                    return false;
                    // exec('kill %1', function(status, output) { 
                    // });


                }

                downloadAllLinkPage(i, _currentYear - 1, _elem, timerWait, _isLastIndex);

            }, (  finalTimer));



    }
