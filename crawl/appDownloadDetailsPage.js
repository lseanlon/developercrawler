 var cheerio = require('cheerio');
 var fs = require('fs');
 var download = require('download');
 // var sleeper = require('sleep');
 var mkdirp = require('mkdirp');
 require('shelljs/global');
 var generalUrlHeader = '-H "Pragma: no-cache" -H "Accept-Encoding: gzip, deflate, sdch, br" -H "Accept-Language: en-US,en;q=0.8" -H "Upgrade-Insecure-Requests: 1" -H "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.1883.87 Safari/537.36" -H "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8" -H "Cache-Control: no-cache" -H "Cookie: logged_in=no; _octo=GH1.1.1831883623.1486184331; _gh_sess=; _ga=GA1.2.291845938.1486184330; _gat=1; tz=Asia%2FKuala_Lumpur" -H "Connection: keep-alive" --compressed'

 var jsonListIndexStr = fs.readFileSync("./dist/detailLinkIndex.txt", 'utf8');
 var jsonIndexList = JSON.parse(jsonListIndexStr);
 var listCommandRun = [];
 var getUrlEntry = function() {
     var paramValue = './src/configuration/UrlList.txt';
     process.argv.forEach(function(val, index, array) {

         // node app  src/UrlList.txt 
         // eg. [0] node / [1] app/ [2]'src/UrlListInfo.txt'
         //console.log(index + ': ' + val);  

         //map file name
         if (index == 2 && val) {
             paramValue = val;
         }

     });

     return paramValue;
 };

 var urlEntry = fs.readFileSync(getUrlEntry(), 'utf8');
 urlList = urlEntry.split("\n");
 var outputJson = {};


 //timer in second
 var timerWait = fs.readFileSync('./src/configuration/TimerInfo.txt', 'utf8');
 timerWait = parseInt(timerWait) * 1000;


 var listOfLink = {};
 //download all the listing links
 urlList.forEach(function(_elem, index, collection) {
     var urlInfo = _elem.split("|");
     var urlPath = urlInfo[0];
     var urlLink = urlInfo[1];
     var ref = urlInfo[2];


     var randomNum = Math.floor(Math.random() * 574) + 1;
     var list = jsonIndexList[ref];
     for (var i = 0; i <= list.length; i++) {
         if (list[i]) {
             downloadAllLinkPage(i, list[i], timerWait + randomNum, (i) == list.lengthz)
         }
     }


 });

 function exitHandler(options, err) {
     if (options.cleanup) console.log('clean');
     if (err) console.log(err.stack);
     if (options.exit) process.exit();
 }

 function downloadAllLinkPage(i, _elem, timerWait, _isLastIndex) {
     var extraTimer = 0;

     if (!_elem) {
         return;
     }

     var urlLinkPage = _elem.link;
     var outputfolderpath = "./dist/FinalPage/" + _elem.ref + "/";
     fs.stat(outputfolderpath, function(err, stats) {
         if (err) {
             mkdirp(outputfolderpath, function(err) {
                 if (err) return cb(err);
                 console.log("create folder path", outputfolderpath);
             });
         }

     });


     var outputfilepath = outputfolderpath + i + ".txt";

     if (fs.existsSync(outputfilepath)) {
         extraTimer = 0;
     } else {
         extraTimer = 2650;
     }
     var finalTimer = i * extraTimer
     if (i > 60 || finalTimer > 120000) {
         finalTimer = 60 * extraTimer
         finalTimer = 4.556 * 1000;
     }


     var curRow = { 'urlLinkPage': urlLinkPage, 'outputfilepath': outputfilepath };

     if (!listOfLink[_elem.ref]) {
         listOfLink[_elem.ref] = [];
     }
     console.log('curRow', curRow);
     listOfLink[_elem.ref].push(curRow);
     _isLastIndex = true;
     if (_isLastIndex) {}
     var pathname = "./dist/downloadedFinalDetails-" + _elem.ref + ".txt"
     console.log("Write download result to path ", pathname);
     fs.writeFileSync(pathname, JSON.stringify(listOfLink));


     // sleeper.sleep(1);
     var timerval = (i * 1.65) > 30 ? 12.12 : (i * 1.65);
     var sleepTimer = " sleep " + timerval + "s ";
     var sleepTimerEnd = " sleep " + timerval + "s ";
     var commandRun = sleepTimer + '  && curl "' + (urlLinkPage) + '" ' + generalUrlHeader + ' > "' + outputfilepath + '" &&   ' + sleepTimerEnd;
     listCommandRun.push(commandRun);
     fs.writeFileSync('./dist/appDownloadDetailsPage.Command.txt', listCommandRun);


     if (fs.existsSync(outputfilepath)) {
         console.log("File downloaded. Already found. Skipped downloading ", urlLinkPage);
         console.log("File downloaded. Skipped downloading path ", outputfilepath);

     } else {

         setTimeout(function() {


             console.log("start downloading ", urlLinkPage);

             try {


                 console.log("commandRun ", commandRun);
                 exec(commandRun, function(status, output) {
                     console.log('done download - ' + urlLinkPage + ' into ' + outputfilepath);

                 });

             } catch (e) {
                 console.log(e);

             }

             // var sleepTimer = " sleep " + (3.453) + "s ";
             // var sleepTimerEnd = " sleep " + (3.111) + "s ";
             // var commandRun = sleepTimer + '&& phantomjs PageDownload6.js "' + (urlLinkPage) + '" &&' + sleepTimerEnd;
             // try {

             //     exec(commandRun, function(status, output) {
             //         console.log('Exit status:', status);
             //         console.log('Program output:');

             //         if (!urlLinkPage || !output) {
             //             return;
             //         }

             //         fs.writeFileSync(outputfilepath, output);
             //         console.log('done download - ' + urlLinkPage + ' into ' + outputfilepath);

             //     });

             // } catch (e) {
             //     console.log(e);

             // }

         }, (finalTimer));
     }


 }
