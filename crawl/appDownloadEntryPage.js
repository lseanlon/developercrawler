var cheerio = require('cheerio');
var fs = require('fs');
var download = require('download');
// var sleeper = require('sleep');
var mkdirp = require('mkdirp');
var daterange = "";
require('shelljs/global');
var downloadPath = fs.readFileSync('./src/configuration/EntryPathInfo.txt', 'utf8');
var outputJson = {};      
var generalUrlHeader =  '-H "Pragma: no-cache" -H "Accept-Encoding: gzip, deflate, sdch, br" -H "Accept-Language: en-US,en;q=0.8" -H "Upgrade-Insecure-Requests: 1" -H "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.1883.87 Safari/537.36" -H "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8" -H "Cache-Control: no-cache" -H "Cookie: logged_in=no; _octo=GH1.1.1831883623.1486184331; _gh_sess=; _ga=GA1.2.291845938.1486184330; _gat=1; tz=Asia%2FKuala_Lumpur" -H "Connection: keep-alive" --compressed'
var getUrlEntry = function() {

    var paramValue = './src/configuration/UrlList.txt';
    process.argv.forEach(function(val, index, array) {

        // node app  src/UrlList.txt 
        // eg. [0] node / [1] app/ [2]'src/UrlListInfo.txt'
        //console.log(index + ': ' + val);  

        //map file name
        if (index == 2 && val) {
            paramValue = val;
        }
        if (index == 3 && val) {
            daterange = val;
        }

    });

    return paramValue;
};

var urlEntry = fs.readFileSync(getUrlEntry(), 'utf8');
urlList = urlEntry.split("\n");

//timer in second
var timerWait = fs.readFileSync('./src/configuration/TimerInfo.txt', 'utf8');
timerWait = parseInt(timerWait) * 1000;

Date.prototype.yyyymmdd = function() {
    var mm = this.getMonth() + 1; // getMonth() is zero-based
    var dd = this.getDate();

    return [this.getFullYear(),
        (mm > 9 ? '' : '0') + mm,
        (dd > 9 ? '' : '0') + dd
    ].join('-');
};

var todayDate = new Date();
todayDate = todayDate.yyyymmdd();

var downloadedEntryPathList = [];
var outputfolderpath = "";
//download all the first page listing
urlList.forEach(function(_elem, index, collection) {

    if (!_elem) {
        return;
    }

    var urlInfo = _elem.split("|");
    var urlPath = urlInfo[0];
    var urlLink = urlInfo[1].replace(/\[TODAY_DATE\]/gi, todayDate);
    if (!daterange) {

        var newDate = new Date();
        newDate.setDate(newDate.getDate() - 3);
        var defaultDate = newDate.yyyymmdd() + ".." + todayDate
            // 'today-3..today'
        urlLink = urlLink.replace(/\[daterange\]/gi, defaultDate);
        console.log('Missing date range using DEFAULT ', defaultDate);
    } else {
        //tablulate value  
        var regex = /today*[+-][0-9]*/i;
        while (curMatch = daterange.match(regex)) {
            curMatch = curMatch[0]
            console.log(curMatch);
            var valuePart = parseFloat(curMatch.replace('today', ''));

            var temDate = new Date();
            temDate.setDate(temDate.getDate() + valuePart);
            var tabulatedDate = temDate.yyyymmdd();
            daterange = daterange.replace(curMatch, tabulatedDate);
        }

        daterange = daterange.replace(/today/gi, todayDate);
        urlLink = urlLink.replace(/\[daterange\]/gi, daterange);
    }



    var indexAddStrOpen = urlLink.indexOf('(ADD');
    if (indexAddStrOpen > -1) {
        var indexAddStrClose = urlLink.indexOf(')', indexAddStrOpen);
        var parameterAddDateStr = urlLink.substring(indexAddStrOpen + 1, indexAddStrClose);

        // (ADD,[TODAY_DATE],+120)
        parameterAddDateList = parameterAddDateStr.split(',');
        console.log('parameterAddDateList', parameterAddDateList);
        var newDate = new Date(parameterAddDateList[1]);
        newDate.setDate(newDate.getDate() + parseFloat(parameterAddDateList[2]));
        urlLink = urlLink.replace('(' + parameterAddDateStr + ')', newDate.yyyymmdd());
    }

    console.log('urlLink', urlLink);
    var ref = urlInfo[2];
    var curRow = { 'ref': ref, 'urlLink': urlLink, 'urlPath': urlPath, 'downloadPath': downloadPath };
    console.log('  curRow', curRow);

    //add random number to timer
    var randomNum = Math.floor(Math.random() * 574) + 1;

    if (fs.existsSync(urlPath)) {
        randomNum = -3000;
    }
    var finaltimer = index * timerWait + randomNum
    if (finaltimer > 120000) {
        finaltimer = 6.23 * 1000;
    }
    if (fs.existsSync(urlPath)) {
        finaltimer = -20;
    }
    outputfolderpath = "./dist/EntryPage"
    mkdirp(outputfolderpath, function(err) {
        if (err) return cb(err);
        console.log("create folder path", outputfolderpath);
    });


    //add timeout - reduce suspicious of crawling
    setTimeout(function() {

        console.log('Check exist ');
        // if ( fs.existsSync(urlPath)) {
        //     console.log('File downloaded found.Skipping', urlPath);
        // } else {  }

        console.log('start download ', urlLink);

        // sleeper.sleep(1); 
        try {
            var commandRun =   ' sleep 1.4s  && curl "' + (urlLink) +  '" '+ generalUrlHeader +' > "' + urlPath + '"   && sleep 1.2s'  ;
            console.log('commandRun',commandRun);

            exec(commandRun, function(status, output) {

                console.log('done download urlLink', urlLink);
                console.log('done download urlPath', urlPath);
                handleSubPages(curRow);
 
            });

        } catch (e) {
            console.log(e);

        }



        // var commandRun = 'phantomjs PageDownload6.js "' + (urlLink) + '"';
        // try {

        //     exec(commandRun, function(status, output) {
        //         console.log('Exit status:', status);
        //         console.log('Program output:');

        //         console.log('done download urlLink', urlLink);
        //         console.log('done download urlPath', urlPath);



        //         if (!urlPath || !output) {
        //             return;
        //         }

        //         fs.writeFileSync(urlPath, output);
        //         handleSubPages(curRow);


        //     });

        // } catch (e) {
        //     console.log(e);

        // }
        downloadedEntryPathList.push(curRow);



    }, finaltimer);

});

var handleSubPages = function(_elem) {


    console.log('handleSubPages');
    //each first page lisitng, download al the sub pages 
    var randomNum = null;

    //add random number to timer
    randomNum = Math.floor(Math.random() * 574) + 1;


    var fileContent = fs.readFileSync(_elem.urlPath, 'utf8');

    var $ = cheerio.load(fileContent);


    //divide no of record over pagination, use ceiling
    var numberOfPage = 1
    if ($('.next_page').prev()) {
        numberOfPage = $('.next_page').prev().html();
        numberOfPage = parseFloat(numberOfPage)
        numberOfPage = isNaN(numberOfPage) ? 1 : numberOfPage;
    }
    console.log('numberOfPage', numberOfPage);

    // determine record per page
    var listingPerPage = 10;
    $('.user-list-item').length;
    console.log('listingPerPage', listingPerPage);

    //determine number of record
    // var totalrec =  0
    // if($('.page-title').eq(0)){ 
    //     totalrec =  $('.page-title').eq(0).html().replace('Jobs Found' ,'')
    //     totalrec = parseFloat(totalrec) 
    //     totalrec= isNaN(totalrec) ? 0:totalrec;  
    // }

    // console.log('totalrec', totalrec);


    var filePathFormat = _elem.downloadPath + "pages-" + _elem.ref;
    outputJson = { "numberOfPage": numberOfPage, "ref": _elem.ref, "filePathFormat": filePathFormat };
    fs.appendFileSync('./dist/listingIndex.txt', JSON.stringify(outputJson) + ",");

    // //download all the page 
    for (var i = 1; i <= numberOfPage; i++) {
        downloadAllListPages(i, _elem, timerWait + randomNum)
    }



};

function removeURLParameter(url, parameter) {
    //prefer to use l.search if you have a location/link object
    var urlparts = url.split('?');
    if (urlparts.length >= 2) {

        var prefix = encodeURIComponent(parameter) + '=';
        var pars = urlparts[1].split(/[&;]/g);

        //reverse iteration as may be destructive
        for (var i = pars.length; i-- > 0;) {
            //idiom for string.startsWith
            if (pars[i].lastIndexOf(prefix, 0) !== -1) {
                pars.splice(i, 1);
            }
        }

        url = urlparts[0] + '?' + pars.join('&');
        return url;
    } else {
        return url;
    }
}

function downloadAllListPages(i, _elem, timerWait) {
    // _elem.urlLink = _elem.urlLink.replace("&p=", "");
    // var urlLinkPage = _elem.urlLink + "&p=2" + i;    
    _elem.urlLink = removeURLParameter(_elem.urlLink, 'p')
    var urlLinkPage = _elem.urlLink;
    //already has param string
    if (_elem.urlLink && (_elem.urlLink.indexOf("?") > -1 || _elem.urlLink.indexOf("&") > -1)) {
        urlLinkPage = urlLinkPage + "&p=" + i;
    } else {
        urlLinkPage = urlLinkPage + "?p=" + i;
    }

    var urlLinkDownloadPage = _elem.downloadPath + "pages-" + _elem.ref + i + ".txt";
    mkdirp(_elem.downloadPath, function(err) {
        if (err) return cb(err);
        console.log("create folder path", _elem.downloadPath);
    });

    var newnum = 0;
    if (fs.existsSync(urlLinkDownloadPage)) {
        newnum = -3000;
    }
    var finaltimer = i * 3000 - newnum;
    if (fs.existsSync(urlLinkDownloadPage)) {
        finaltimer = -20;
    }

    setTimeout(function() {
        if (fs.existsSync(urlLinkDownloadPage)) {
            console.log('File downloaded found. Skipping download', urlLinkPage);
        } else {
            console.log('urlLinkDownloadPage', urlLinkDownloadPage);
            console.log('urlLinkPage', urlLinkPage);
            console.log(' _elem.downloadPath', _elem.downloadPath);

            // sleeper.sleep(1);
            var timerval =(i * 1.65)>30 ? 12.2:(i * 1.65) ;
            var sleepTimer = " sleep " + timerval+ "s ";
            var sleepTimerEnd = " sleep " + timerval + "s ";
            try {
                var commandRun = sleepTimer + '  && curl "' + (urlLinkPage) +  '" '+generalUrlHeader +' > "' + urlLinkDownloadPage + '" &&   ' + sleepTimerEnd;
                console.log('commandRun',commandRun);;
                exec(commandRun, function(status, output) {
                    console.log(' Exit status:', status);
                    console.log('done download - ' + _elem.urlLink + ' into ' + _elem.urlPath);

                });

            } catch (e) {
                console.log(e);

            }

            // var sleepTimer = " sleep " + (i * 0.65) + "s ";
            // var sleepTimerEnd = " sleep " + (i * 0.74) + "s ";
            // try {
            //     var commandRun = sleepTimer + '  && phantomjs PageDownload6.js "' + (urlLinkPage) + '" &&   ' + sleepTimerEnd;

            //     exec(commandRun, function(status, output) {
            //         console.log('Exit status:', status);
            //         console.log('done download - ' + _elem.urlLink + ' into ' + _elem.urlPath);

            //         if (!urlLinkDownloadPage || !output) {
            //             return;
            //         }

            //         fs.writeFileSync(urlLinkDownloadPage, output);


            //     });

            // } catch (e) {
            //     console.log(e);

            // }

        }
    }, finaltimer);
}
