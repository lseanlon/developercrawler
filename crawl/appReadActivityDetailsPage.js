var cheerio = require('cheerio');
var fs = require('fs');
var download = require('download');


var downloadPath = fs.readFileSync('./src/configuration/EntryPathInfo.txt', 'utf8');
var outputJson = {};
var getUrlEntry = function() {

    var paramValue = './src/configuration/UrlList.txt';
    process.argv.forEach(function(val, index, array) {

        // node app  src/UrlList.txt 
        // eg. [0] node / [1] app/ [2]'src/UrlListInfo.txt'
        //console.log(index + ': ' + val);  

        //map file name
        if (index == 2 && val) {
            paramValue = val;
        }

    });

    return paramValue;
};

// read downloadedFinalDetails file
// parse json
// loop entire folder, read entire index file
// scrap out all row into json
// save into output file

var urlEntry = fs.readFileSync(getUrlEntry(), 'utf8');
urlList = urlEntry.split("\n");

//timer in second
var timerWait = fs.readFileSync('./src/configuration/TimerInfo.txt', 'utf8');
timerWait = parseInt(timerWait) * 1000;

var striphtml = function(html) {
    var tmp = document.createElement("DIV");
    tmp.innerHTML = html;
    return tmp.textContent || tmp.innerText || "";
}
var chunks = require('chunk-array').chunks
var finalResultList = [];
var finalResultExcelList = [];
var downloadedEntryPathList = [];
//download all the first page listing
var ref = '';



urlList.forEach(function(_elem, index, collection) {
    var urlInfo = _elem.split("|");
    ref = urlInfo[2];

    var jsonFileDetails;
    var filefullnamepath = './dist/downloadedFinalUserDetails-' + ref + '.txt'
    try {
        jsonFileDetails = fs.readFileSync(filefullnamepath, 'utf8');
        jsonFileDetails = JSON.parse(jsonFileDetails);

    } catch (e) {
        console.log('SKIPPED READ BECAUSE DOWNLOAD FAILED FOR ', filefullnamepath);
        return;
    }

    var listRef = jsonFileDetails;
    for (var i = 0; i <= listRef.length; i++) {
        try {


            if (listRef[i]) {

                var elem = listRef[i];
                console.log("Reading file ", elem["outputfilepath"]);
                var filehtml = fs.readFileSync(elem["outputfilepath"], 'utf8');
                var $ = cheerio.load(filehtml);



                var sanitizeDash = function(_val) {
                    //take out all html   
                    return ($("<p>" + _val + "</p>").text());
                }

                //remove non activity

                $('rect[fill="#eeeeee"]').remove()


                console.log('LENGTH', $('rect').length);
                var contribString = "";
                $('rect').each(function(iddx, element) {
                    var curDate = "|" + $('rect')[iddx].attribs['data-date'];
                    // var curDate=  "|" +  $('element').html() ;
                    contribString += curDate;

                });
                elem.contribution = contribString;
                // console.log(contribString);

               
                console.log("elem ", elem);
                if (!finalResultExcelList[ref]) { finalResultExcelList[ref] = []; }
                var elemIsUnique = true;
                for (var temidx = 0; temidx < finalResultList.length; temidx++) {

                    if (elem && elem.username && elem.year && elem.username == finalResultList[temidx].username && elem.year == finalResultList[temidx].year) {
                        elemIsUnique = false;
                        return false;
                    }

                }
                if (elemIsUnique) {
                    finalResultList.push(elem);
                    finalResultExcelList[ref].push(elem);
                }
                 var filepath = './dist/recordFinalUserDetails-' + ref + '.txt'
                console.log("Write to file ", filepath);
                fs.writeFileSync(filepath, JSON.stringify(finalResultList));

                var filepath = '../shared/UserContribution-' + ref + '.txt'
                console.log("Write to file ", filepath);
                fs.writeFileSync(filepath, JSON.stringify(finalResultList));

            }

        } catch (e) {
            console.log(e);
        }
    }

    if (urlList.length == (index + 1)) {}


});




//write into excel 
var json2xls = require('json2xls');
for (var key in finalResultExcelList) {
    if (finalResultExcelList.hasOwnProperty(key)) {
        // console.log(key + " -> " + finalResultExcelList[key]);  
        var xls = json2xls(finalResultExcelList[key]);
        var outexcelpath = './dist/' + key + '-UserData.xlsx';
        console.log("Write to excel ", outexcelpath);
        fs.writeFileSync(outexcelpath, xls, 'binary');
    }
}
