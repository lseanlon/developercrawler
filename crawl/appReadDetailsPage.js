var cheerio = require('cheerio');
var fs = require('fs');
var download = require('download');


var downloadPath = fs.readFileSync('./src/configuration/EntryPathInfo.txt', 'utf8');
var outputJson = {};
var getUrlEntry = function() {

    var paramValue = './src/configuration/UrlList.txt';
    process.argv.forEach(function(val, index, array) {

        // node app  src/UrlList.txt 
        // eg. [0] node / [1] app/ [2]'src/UrlListInfo.txt'
        //console.log(index + ': ' + val);  

        //map file name
        if (index == 2 && val) {
            paramValue = val;
        }

    });

    return paramValue;
};

// read downloadedFinalDetails file
// parse json
// loop entire folder, read entire index file
// scrap out all row into json
// save into output file

var urlEntry = fs.readFileSync(getUrlEntry(), 'utf8');
urlList = urlEntry.split("\n");

//timer in second
var timerWait = fs.readFileSync('./src/configuration/TimerInfo.txt', 'utf8');
timerWait = parseInt(timerWait) * 1000;

var striphtml = function(html) {
    var tmp = document.createElement("DIV");
    tmp.innerHTML = html;
    return tmp.textContent || tmp.innerText || "";
}
var chunks = require('chunk-array').chunks
var finalResultList = [];
var finalResultExcelList = [];
var downloadedEntryPathList = [];
//download all the first page listing
var ref = '';

try {

    urlList.forEach(function(_elem, index, collection) {
        var urlInfo = _elem.split("|");
        ref = urlInfo[2];

        var jsonFileDetails;
        var filefullnamepath = './dist/downloadedFinalDetails-' + ref + '.txt'
        try {
            jsonFileDetails = fs.readFileSync(filefullnamepath, 'utf8');
            jsonFileDetails = JSON.parse(jsonFileDetails);

        } catch (e) {
            console.log('SKIPPED READ BECAUSE DOWNLOAD FAILED FOR ', filefullnamepath);
            return;
        }



        var listRef = jsonFileDetails[ref];
        for (var i = 0; i <= listRef.length; i++) {
            if (listRef[i]) {

                var elem = listRef[i];
                console.log("Reading file ", elem["outputfilepath"]);
                var filehtml = fs.readFileSync(elem["outputfilepath"], 'utf8');
                var $ = cheerio.load(filehtml);

                var currentRow = {};
                var jobDesc = "";
                var rawJobdesc = '';
                var isView = true;
                var BulletList = [];


                var sanitizeDash = function(_val) {
                    //take out all html   
                    return ($("<p>" + _val + "</p>").text());
                }


                currentRow['ref'] = ref;
                //contactName
                if ($(".vcard-fullname")) {
                    currentRow['fullname'] = $(".vcard-fullname").html()
                }

                if ($(".vcard-username")) {
                    currentRow['username'] = $(".vcard-username").html()
                }

                currentRow['company'] = "";
                if ($("[itemprop='worksFor']")) {
                    currentRow['company'] = sanitizeDash($("[itemprop='worksFor']").html())
                }

                $('.octicon').remove();
                currentRow['location'] = "";
                if ($("[itemprop='homeLocation']")) {
                    currentRow['location'] = sanitizeDash($("[itemprop='homeLocation']").html())
                }
                currentRow['email'] = "";
                if ($("[itemprop='email']")) {
                    currentRow['email'] = sanitizeDash($("[itemprop='email']").html())
                }

                currentRow['specialist'] = "";
                $('.pinned-repo-item-content').each(function(iddxx, elemm) {
                    if ($(this).find('p:nth-child(3)')) {
                        var $content = $(this).find('p:nth-child(3)');
                        $content.remove('span').remove('a');
                        currentRow['specialist'] = currentRow['specialist'] + " / " + sanitizeDash($content.html())
                    }

                });

                if (!currentRow['location'] && $("[itemprop='location']")) {
                    currentRow['location'] = sanitizeDash($("[itemprop='location']").html())
                }
                if (!currentRow['fullname'] && $(".org-name")) {
                    currentRow['fullname'] = $(".org-name").html()
                    currentRow['company'] = $(".org-name").html()
                }

                if (!currentRow['username'] && $(".pagehead-tabs-item.selected").attr('href')) {
                    currentRow['username'] = $(".pagehead-tabs-item.selected").attr('href').replace('/', '')
                }
                currentRow['specialist'] = currentRow['specialist'].replace('\n', '').replace('\r', '').replace('  ', ' ');

                currentRow['totalFollowing'] = 0

                currentRow['totalFollowers'] = 0

                currentRow['totalStars'] = 0

                currentRow['totalRepo'] = 0;



                $('.counter').each(function(iddx, element) {
                    if ($(this).parent() && $(this).parent().html()) {
                        var currentHtml = $(this).html();
                        var parentHtml = $(this).parent().html().toLowerCase();
                        if (parentHtml.indexOf('star') > -1) {
                            currentRow['totalStars'] = currentHtml;
                        }
                        if (parentHtml.indexOf('follower') > -1) {
                            currentRow['totalFollowers'] = currentHtml;
                        }
                        if (parentHtml.indexOf('people') > -1) {
                            currentRow['totalFollowers'] = sanitizeDash(currentHtml);
                        }
                        if (parentHtml.indexOf('repo') > -1) {
                            currentRow['totalRepo'] = currentHtml;
                        }
                        if (parentHtml.indexOf('following') > -1) {
                            currentRow['totalFollowing'] = currentHtml;
                        }

                    }

                });


            }



            var filepath = './dist/recordFinalDetails-' + ref + '.txt'
            console.log("Write to file ", filepath);
            fs.writeFileSync(filepath, JSON.stringify(finalResultList));


            var filepath = '../shared/User-' + ref + '.txt'
            console.log("Write to file ", filepath);
            fs.writeFileSync(filepath, JSON.stringify(finalResultList));

            // console.log("currentRow ", currentRow);
            if (!finalResultExcelList[ref]) { finalResultExcelList[ref] = []; }
            var elemIsUnique = true;
            var listStr = JSON.stringify(finalResultList);
            if (listStr.indexOf(currentRow.username) == -1) {

                finalResultList.push(currentRow);
                finalResultExcelList[ref].push(currentRow);
            } else {
                console.log('EXCLUDED ROW because NOT UNQUE', currentRow);
            }

        }

        if (urlList.length == (index + 1)) {}


    });


} catch (e) {
    console.log(e);
}


//write into excel 
var json2xls = require('json2xls');
for (var key in finalResultExcelList) {
    if (finalResultExcelList.hasOwnProperty(key)) {
        // console.log(key + " -> " + finalResultExcelList[key]);  
        var xls = json2xls(finalResultExcelList[key]);
        var outexcelpath = './dist/' + key + '-data.xlsx';
        console.log("Write to excel ", outexcelpath);
        fs.writeFileSync(outexcelpath, xls, 'binary');
    }
}
