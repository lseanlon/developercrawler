CREATE TABLE `developer` (
   `id` int(10) unsigned not null auto_increment,
   `source` varchar(255), #slidshare or github
   `fullname` varchar(255), #developer fullnamename
   `username` varchar(255), #developer username name
   `company` varchar(255), #developer company name
   `email` varchar(255), #developer email
   `location` varchar(255), #developer location
   `specialist` varchar(255), #developer specialist
   `totalFollowing` varchar(255), #developer totalFollowing
   `totalFollowers` varchar(255), #developer totalFollowers
   `totalStars` varchar(255), #developer totalStars
   `totalRepo` varchar(255), #developer totalRepo  

   `web` varchar(255),
   `facebook` varchar(255),
   `linkedin` varchar(255),
   `twitter` varchar(255),

   `totalDocument` varchar(255),
   `totalInfographics` varchar(255),
   `totalPresentationSlides` varchar(255),
   `totalVideo` varchar(255), 

   `createdby` VARCHAR( 100 ) ,   
   `createddate`  DATETIME( 3 )   , 
   PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

  CREATE TABLE `developercontribution` (
   `id` int(10) unsigned not null auto_increment,
   `developerid` int(10)  ,
   `year` varchar(255), #slidshare or github
   `contributiondates` varchar(5000), #developer fullnamename 
   `createdby` VARCHAR( 100 ) ,   
   `createddate`  DATETIME( 3 )   , 
   PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;



  CREATE TABLE `developerslide` (
   `id` int(10) unsigned not null auto_increment,
   `developerid` int(10)  ,
   `slidecontent` varchar(8000),  
   `slidelinks` varchar(8000),  
   `createdby` VARCHAR( 100 ) ,   
   `createddate`  DATETIME( 3 )   , 
   PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;
