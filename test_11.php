<?php 

require 'application/Slim/Slim.php';
\Slim\Slim::registerAutoloader();
$app = new \Slim\Slim(); $app->get('/', function () use ($app) {
	require_once 'lib/mysql.php';
	$db = connect_db();
	$result = $db->query( 'SELECT id, name, job FROM developer;' );
	while ( $row = $result->fetch_array(MYSQLI_ASSOC) ) {
		$data[] = $row;
	}

	$app->render('list.php', array(
			'page_title' => "Your List",
			'data' => $data
		)
	);
});

$app->run();

function connect_db() {
	$server = 'http://153.127.197.95/'; // this may be an ip address instead
	$user = 'developer';
	$pass = 'tB3QgjdR';
	$database = 'developercrawler';
	$connection = new mysqli($server, $user, $pass, $database);

	return $connection;
}
?>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title><?php echo $this->data['page_title']; ?></title>
</head>
<body>
<?php
foreach ($this->data['data'] as $friend) {
	echo $friend['id'].' - '.$friend['name'].' - '.$friend['job'].'</br />';
}
?>
</body>
</html>
